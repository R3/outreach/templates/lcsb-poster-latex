
# LCSB poster templates in LaTeX

This includes:

- `classic.tex` for the "normal" template
- `better.tex` for a slightly #betterposter-adjusted template

You can build both posters using `latexmk`.
